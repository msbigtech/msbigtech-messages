// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             (unknown)
// source: v1/service.proto

package api

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	MessagesService_CreateUser_FullMethodName                       = "/gitlab.com.msbigtech.msbigtech_messages.MessagesService/CreateUser"
	MessagesService_DeleteUser_FullMethodName                       = "/gitlab.com.msbigtech.msbigtech_messages.MessagesService/DeleteUser"
	MessagesService_CreateServer_FullMethodName                     = "/gitlab.com.msbigtech.msbigtech_messages.MessagesService/CreateServer"
	MessagesService_DeleteServer_FullMethodName                     = "/gitlab.com.msbigtech.msbigtech_messages.MessagesService/DeleteServer"
	MessagesService_CreateServerChannel_FullMethodName              = "/gitlab.com.msbigtech.msbigtech_messages.MessagesService/CreateServerChannel"
	MessagesService_DeleteServerChannel_FullMethodName              = "/gitlab.com.msbigtech.msbigtech_messages.MessagesService/DeleteServerChannel"
	MessagesService_InviteUserToServer_FullMethodName               = "/gitlab.com.msbigtech.msbigtech_messages.MessagesService/InviteUserToServer"
	MessagesService_SendMessageToServerChannel_FullMethodName       = "/gitlab.com.msbigtech.msbigtech_messages.MessagesService/SendMessageToServerChannel"
	MessagesService_SendMessageToUser_FullMethodName                = "/gitlab.com.msbigtech.msbigtech_messages.MessagesService/SendMessageToUser"
	MessagesService_ReceiveMessagesFromServerChannel_FullMethodName = "/gitlab.com.msbigtech.msbigtech_messages.MessagesService/ReceiveMessagesFromServerChannel"
	MessagesService_ReceiveMessagesFromUser_FullMethodName          = "/gitlab.com.msbigtech.msbigtech_messages.MessagesService/ReceiveMessagesFromUser"
)

// MessagesServiceClient is the client API for MessagesService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type MessagesServiceClient interface {
	CreateUser(ctx context.Context, in *CreateUserRequest, opts ...grpc.CallOption) (*CreateUserResponse, error)
	DeleteUser(ctx context.Context, in *DeleteUserRequest, opts ...grpc.CallOption) (*DeleteUserResponse, error)
	CreateServer(ctx context.Context, in *CreateServerRequest, opts ...grpc.CallOption) (*CreateServerResponse, error)
	DeleteServer(ctx context.Context, in *DeleteServerRequest, opts ...grpc.CallOption) (*DeleteServerResponse, error)
	CreateServerChannel(ctx context.Context, in *CreateServerChannelRequest, opts ...grpc.CallOption) (*CreateServerChannelResponse, error)
	DeleteServerChannel(ctx context.Context, in *DeleteServerChannelRequest, opts ...grpc.CallOption) (*DeleteServerChannelResponse, error)
	InviteUserToServer(ctx context.Context, in *InviteUserToServerRequest, opts ...grpc.CallOption) (*InviteUserToServerResponse, error)
	SendMessageToServerChannel(ctx context.Context, in *SendMessageToServerChannelRequest, opts ...grpc.CallOption) (*SendMessageToServerChannelResponse, error)
	SendMessageToUser(ctx context.Context, in *SendMessageToUserRequest, opts ...grpc.CallOption) (*SendMessageToUserResponse, error)
	ReceiveMessagesFromServerChannel(ctx context.Context, in *ReceiveMessagesFromServerChannelRequest, opts ...grpc.CallOption) (*ReceiveMessagesFromServerChannelResponse, error)
	ReceiveMessagesFromUser(ctx context.Context, in *ReceiveMessagesFromUserRequest, opts ...grpc.CallOption) (*ReceiveMessagesFromUserResponse, error)
}

type messagesServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewMessagesServiceClient(cc grpc.ClientConnInterface) MessagesServiceClient {
	return &messagesServiceClient{cc}
}

func (c *messagesServiceClient) CreateUser(ctx context.Context, in *CreateUserRequest, opts ...grpc.CallOption) (*CreateUserResponse, error) {
	out := new(CreateUserResponse)
	err := c.cc.Invoke(ctx, MessagesService_CreateUser_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagesServiceClient) DeleteUser(ctx context.Context, in *DeleteUserRequest, opts ...grpc.CallOption) (*DeleteUserResponse, error) {
	out := new(DeleteUserResponse)
	err := c.cc.Invoke(ctx, MessagesService_DeleteUser_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagesServiceClient) CreateServer(ctx context.Context, in *CreateServerRequest, opts ...grpc.CallOption) (*CreateServerResponse, error) {
	out := new(CreateServerResponse)
	err := c.cc.Invoke(ctx, MessagesService_CreateServer_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagesServiceClient) DeleteServer(ctx context.Context, in *DeleteServerRequest, opts ...grpc.CallOption) (*DeleteServerResponse, error) {
	out := new(DeleteServerResponse)
	err := c.cc.Invoke(ctx, MessagesService_DeleteServer_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagesServiceClient) CreateServerChannel(ctx context.Context, in *CreateServerChannelRequest, opts ...grpc.CallOption) (*CreateServerChannelResponse, error) {
	out := new(CreateServerChannelResponse)
	err := c.cc.Invoke(ctx, MessagesService_CreateServerChannel_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagesServiceClient) DeleteServerChannel(ctx context.Context, in *DeleteServerChannelRequest, opts ...grpc.CallOption) (*DeleteServerChannelResponse, error) {
	out := new(DeleteServerChannelResponse)
	err := c.cc.Invoke(ctx, MessagesService_DeleteServerChannel_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagesServiceClient) InviteUserToServer(ctx context.Context, in *InviteUserToServerRequest, opts ...grpc.CallOption) (*InviteUserToServerResponse, error) {
	out := new(InviteUserToServerResponse)
	err := c.cc.Invoke(ctx, MessagesService_InviteUserToServer_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagesServiceClient) SendMessageToServerChannel(ctx context.Context, in *SendMessageToServerChannelRequest, opts ...grpc.CallOption) (*SendMessageToServerChannelResponse, error) {
	out := new(SendMessageToServerChannelResponse)
	err := c.cc.Invoke(ctx, MessagesService_SendMessageToServerChannel_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagesServiceClient) SendMessageToUser(ctx context.Context, in *SendMessageToUserRequest, opts ...grpc.CallOption) (*SendMessageToUserResponse, error) {
	out := new(SendMessageToUserResponse)
	err := c.cc.Invoke(ctx, MessagesService_SendMessageToUser_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagesServiceClient) ReceiveMessagesFromServerChannel(ctx context.Context, in *ReceiveMessagesFromServerChannelRequest, opts ...grpc.CallOption) (*ReceiveMessagesFromServerChannelResponse, error) {
	out := new(ReceiveMessagesFromServerChannelResponse)
	err := c.cc.Invoke(ctx, MessagesService_ReceiveMessagesFromServerChannel_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *messagesServiceClient) ReceiveMessagesFromUser(ctx context.Context, in *ReceiveMessagesFromUserRequest, opts ...grpc.CallOption) (*ReceiveMessagesFromUserResponse, error) {
	out := new(ReceiveMessagesFromUserResponse)
	err := c.cc.Invoke(ctx, MessagesService_ReceiveMessagesFromUser_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MessagesServiceServer is the server API for MessagesService service.
// All implementations should embed UnimplementedMessagesServiceServer
// for forward compatibility
type MessagesServiceServer interface {
	CreateUser(context.Context, *CreateUserRequest) (*CreateUserResponse, error)
	DeleteUser(context.Context, *DeleteUserRequest) (*DeleteUserResponse, error)
	CreateServer(context.Context, *CreateServerRequest) (*CreateServerResponse, error)
	DeleteServer(context.Context, *DeleteServerRequest) (*DeleteServerResponse, error)
	CreateServerChannel(context.Context, *CreateServerChannelRequest) (*CreateServerChannelResponse, error)
	DeleteServerChannel(context.Context, *DeleteServerChannelRequest) (*DeleteServerChannelResponse, error)
	InviteUserToServer(context.Context, *InviteUserToServerRequest) (*InviteUserToServerResponse, error)
	SendMessageToServerChannel(context.Context, *SendMessageToServerChannelRequest) (*SendMessageToServerChannelResponse, error)
	SendMessageToUser(context.Context, *SendMessageToUserRequest) (*SendMessageToUserResponse, error)
	ReceiveMessagesFromServerChannel(context.Context, *ReceiveMessagesFromServerChannelRequest) (*ReceiveMessagesFromServerChannelResponse, error)
	ReceiveMessagesFromUser(context.Context, *ReceiveMessagesFromUserRequest) (*ReceiveMessagesFromUserResponse, error)
}

// UnimplementedMessagesServiceServer should be embedded to have forward compatible implementations.
type UnimplementedMessagesServiceServer struct {
}

func (UnimplementedMessagesServiceServer) CreateUser(context.Context, *CreateUserRequest) (*CreateUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateUser not implemented")
}
func (UnimplementedMessagesServiceServer) DeleteUser(context.Context, *DeleteUserRequest) (*DeleteUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteUser not implemented")
}
func (UnimplementedMessagesServiceServer) CreateServer(context.Context, *CreateServerRequest) (*CreateServerResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateServer not implemented")
}
func (UnimplementedMessagesServiceServer) DeleteServer(context.Context, *DeleteServerRequest) (*DeleteServerResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteServer not implemented")
}
func (UnimplementedMessagesServiceServer) CreateServerChannel(context.Context, *CreateServerChannelRequest) (*CreateServerChannelResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateServerChannel not implemented")
}
func (UnimplementedMessagesServiceServer) DeleteServerChannel(context.Context, *DeleteServerChannelRequest) (*DeleteServerChannelResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteServerChannel not implemented")
}
func (UnimplementedMessagesServiceServer) InviteUserToServer(context.Context, *InviteUserToServerRequest) (*InviteUserToServerResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method InviteUserToServer not implemented")
}
func (UnimplementedMessagesServiceServer) SendMessageToServerChannel(context.Context, *SendMessageToServerChannelRequest) (*SendMessageToServerChannelResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendMessageToServerChannel not implemented")
}
func (UnimplementedMessagesServiceServer) SendMessageToUser(context.Context, *SendMessageToUserRequest) (*SendMessageToUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendMessageToUser not implemented")
}
func (UnimplementedMessagesServiceServer) ReceiveMessagesFromServerChannel(context.Context, *ReceiveMessagesFromServerChannelRequest) (*ReceiveMessagesFromServerChannelResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReceiveMessagesFromServerChannel not implemented")
}
func (UnimplementedMessagesServiceServer) ReceiveMessagesFromUser(context.Context, *ReceiveMessagesFromUserRequest) (*ReceiveMessagesFromUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReceiveMessagesFromUser not implemented")
}

// UnsafeMessagesServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to MessagesServiceServer will
// result in compilation errors.
type UnsafeMessagesServiceServer interface {
	mustEmbedUnimplementedMessagesServiceServer()
}

func RegisterMessagesServiceServer(s grpc.ServiceRegistrar, srv MessagesServiceServer) {
	s.RegisterService(&MessagesService_ServiceDesc, srv)
}

func _MessagesService_CreateUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagesServiceServer).CreateUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: MessagesService_CreateUser_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagesServiceServer).CreateUser(ctx, req.(*CreateUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MessagesService_DeleteUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagesServiceServer).DeleteUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: MessagesService_DeleteUser_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagesServiceServer).DeleteUser(ctx, req.(*DeleteUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MessagesService_CreateServer_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateServerRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagesServiceServer).CreateServer(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: MessagesService_CreateServer_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagesServiceServer).CreateServer(ctx, req.(*CreateServerRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MessagesService_DeleteServer_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteServerRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagesServiceServer).DeleteServer(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: MessagesService_DeleteServer_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagesServiceServer).DeleteServer(ctx, req.(*DeleteServerRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MessagesService_CreateServerChannel_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateServerChannelRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagesServiceServer).CreateServerChannel(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: MessagesService_CreateServerChannel_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagesServiceServer).CreateServerChannel(ctx, req.(*CreateServerChannelRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MessagesService_DeleteServerChannel_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteServerChannelRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagesServiceServer).DeleteServerChannel(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: MessagesService_DeleteServerChannel_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagesServiceServer).DeleteServerChannel(ctx, req.(*DeleteServerChannelRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MessagesService_InviteUserToServer_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(InviteUserToServerRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagesServiceServer).InviteUserToServer(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: MessagesService_InviteUserToServer_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagesServiceServer).InviteUserToServer(ctx, req.(*InviteUserToServerRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MessagesService_SendMessageToServerChannel_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SendMessageToServerChannelRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagesServiceServer).SendMessageToServerChannel(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: MessagesService_SendMessageToServerChannel_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagesServiceServer).SendMessageToServerChannel(ctx, req.(*SendMessageToServerChannelRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MessagesService_SendMessageToUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SendMessageToUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagesServiceServer).SendMessageToUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: MessagesService_SendMessageToUser_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagesServiceServer).SendMessageToUser(ctx, req.(*SendMessageToUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MessagesService_ReceiveMessagesFromServerChannel_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReceiveMessagesFromServerChannelRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagesServiceServer).ReceiveMessagesFromServerChannel(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: MessagesService_ReceiveMessagesFromServerChannel_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagesServiceServer).ReceiveMessagesFromServerChannel(ctx, req.(*ReceiveMessagesFromServerChannelRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MessagesService_ReceiveMessagesFromUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReceiveMessagesFromUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MessagesServiceServer).ReceiveMessagesFromUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: MessagesService_ReceiveMessagesFromUser_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MessagesServiceServer).ReceiveMessagesFromUser(ctx, req.(*ReceiveMessagesFromUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// MessagesService_ServiceDesc is the grpc.ServiceDesc for MessagesService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var MessagesService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "gitlab.com.msbigtech.msbigtech_messages.MessagesService",
	HandlerType: (*MessagesServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateUser",
			Handler:    _MessagesService_CreateUser_Handler,
		},
		{
			MethodName: "DeleteUser",
			Handler:    _MessagesService_DeleteUser_Handler,
		},
		{
			MethodName: "CreateServer",
			Handler:    _MessagesService_CreateServer_Handler,
		},
		{
			MethodName: "DeleteServer",
			Handler:    _MessagesService_DeleteServer_Handler,
		},
		{
			MethodName: "CreateServerChannel",
			Handler:    _MessagesService_CreateServerChannel_Handler,
		},
		{
			MethodName: "DeleteServerChannel",
			Handler:    _MessagesService_DeleteServerChannel_Handler,
		},
		{
			MethodName: "InviteUserToServer",
			Handler:    _MessagesService_InviteUserToServer_Handler,
		},
		{
			MethodName: "SendMessageToServerChannel",
			Handler:    _MessagesService_SendMessageToServerChannel_Handler,
		},
		{
			MethodName: "SendMessageToUser",
			Handler:    _MessagesService_SendMessageToUser_Handler,
		},
		{
			MethodName: "ReceiveMessagesFromServerChannel",
			Handler:    _MessagesService_ReceiveMessagesFromServerChannel_Handler,
		},
		{
			MethodName: "ReceiveMessagesFromUser",
			Handler:    _MessagesService_ReceiveMessagesFromUser_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "v1/service.proto",
}
