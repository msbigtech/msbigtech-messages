// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.1
// 	protoc        (unknown)
// source: v1/service.proto

package api

import (
	_ "buf.build/gen/go/bufbuild/protovalidate/protocolbuffers/go/buf/validate"
	_ "github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2/options"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	_ "google.golang.org/protobuf/types/known/emptypb"
	reflect "reflect"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

var File_v1_service_proto protoreflect.FileDescriptor

var file_v1_service_proto_rawDesc = []byte{
	0x0a, 0x10, 0x76, 0x31, 0x2f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x12, 0x27, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d,
	0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65,
	0x63, 0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x1a, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x63, 0x2d, 0x67, 0x65, 0x6e, 0x2d, 0x6f, 0x70, 0x65, 0x6e, 0x61, 0x70, 0x69, 0x76,
	0x32, 0x2f, 0x6f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2f, 0x61, 0x6e, 0x6e, 0x6f, 0x74, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1c, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x61, 0x6e, 0x6e, 0x6f, 0x74, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1b, 0x62, 0x75, 0x66, 0x2f, 0x76, 0x61, 0x6c, 0x69,
	0x64, 0x61, 0x74, 0x65, 0x2f, 0x76, 0x61, 0x6c, 0x69, 0x64, 0x61, 0x74, 0x65, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f,
	0x66, 0x69, 0x65, 0x6c, 0x64, 0x5f, 0x62, 0x65, 0x68, 0x61, 0x76, 0x69, 0x6f, 0x72, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x11, 0x76, 0x31, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
	0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x32, 0xbe, 0x0f, 0x0a, 0x0f, 0x4d, 0x65, 0x73, 0x73,
	0x61, 0x67, 0x65, 0x73, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x87, 0x01, 0x0a, 0x0a,
	0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x55, 0x73, 0x65, 0x72, 0x12, 0x3a, 0x2e, 0x67, 0x69, 0x74,
	0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63,
	0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73,
	0x61, 0x67, 0x65, 0x73, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x55, 0x73, 0x65, 0x72, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x3b, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e,
	0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73,
	0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73,
	0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x55, 0x73, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x87, 0x01, 0x0a, 0x0a, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65,
	0x55, 0x73, 0x65, 0x72, 0x12, 0x3a, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f,
	0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69,
	0x67, 0x74, 0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x44,
	0x65, 0x6c, 0x65, 0x74, 0x65, 0x55, 0x73, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x1a, 0x3b, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73,
	0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63,
	0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x44, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x55, 0x73, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12,
	0x8d, 0x01, 0x0a, 0x0c, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72,
	0x12, 0x3c, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73,
	0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63,
	0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x3d,
	0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69,
	0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f,
	0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53,
	0x65, 0x72, 0x76, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12,
	0x8d, 0x01, 0x0a, 0x0c, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72,
	0x12, 0x3c, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73,
	0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63,
	0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x44, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x3d,
	0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69,
	0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f,
	0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x53,
	0x65, 0x72, 0x76, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12,
	0xa2, 0x01, 0x0a, 0x13, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72,
	0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x12, 0x43, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62,
	0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d,
	0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
	0x73, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x43, 0x68,
	0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x44, 0x2e, 0x67,
	0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74,
	0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x65, 0x72,
	0x76, 0x65, 0x72, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x22, 0x00, 0x12, 0xa2, 0x01, 0x0a, 0x13, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x53,
	0x65, 0x72, 0x76, 0x65, 0x72, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x12, 0x43, 0x2e, 0x67,
	0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74,
	0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x53, 0x65, 0x72,
	0x76, 0x65, 0x72, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x44, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d,
	0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65,
	0x63, 0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x44, 0x65, 0x6c, 0x65,
	0x74, 0x65, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0xc3, 0x01, 0x0a, 0x12, 0x49, 0x6e,
	0x76, 0x69, 0x74, 0x65, 0x55, 0x73, 0x65, 0x72, 0x54, 0x6f, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72,
	0x12, 0x42, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73,
	0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63,
	0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x49, 0x6e, 0x76, 0x69, 0x74,
	0x65, 0x55, 0x73, 0x65, 0x72, 0x54, 0x6f, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x43, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f,
	0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69,
	0x67, 0x74, 0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x49,
	0x6e, 0x76, 0x69, 0x74, 0x65, 0x55, 0x73, 0x65, 0x72, 0x54, 0x6f, 0x53, 0x65, 0x72, 0x76, 0x65,
	0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x24, 0x82, 0xd3, 0xe4, 0x93, 0x02,
	0x1e, 0x3a, 0x01, 0x2a, 0x22, 0x19, 0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67,
	0x65, 0x73, 0x2f, 0x75, 0x73, 0x65, 0x72, 0x73, 0x2f, 0x69, 0x6e, 0x76, 0x69, 0x74, 0x65, 0x12,
	0xd7, 0x01, 0x0a, 0x1a, 0x53, 0x65, 0x6e, 0x64, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x54,
	0x6f, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x12, 0x4a,
	0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69,
	0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f,
	0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x53, 0x65, 0x6e, 0x64, 0x4d, 0x65, 0x73,
	0x73, 0x61, 0x67, 0x65, 0x54, 0x6f, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x43, 0x68, 0x61, 0x6e,
	0x6e, 0x65, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x4b, 0x2e, 0x67, 0x69, 0x74,
	0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63,
	0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73,
	0x61, 0x67, 0x65, 0x73, 0x2e, 0x53, 0x65, 0x6e, 0x64, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
	0x54, 0x6f, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x20, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x1a, 0x3a,
	0x01, 0x2a, 0x22, 0x15, 0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73,
	0x2f, 0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x73, 0x12, 0xb9, 0x01, 0x0a, 0x11, 0x53, 0x65,
	0x6e, 0x64, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x54, 0x6f, 0x55, 0x73, 0x65, 0x72, 0x12,
	0x41, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62,
	0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68,
	0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x53, 0x65, 0x6e, 0x64, 0x4d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x65, 0x54, 0x6f, 0x55, 0x73, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x1a, 0x42, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e,
	0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74,
	0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x53, 0x65, 0x6e,
	0x64, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x54, 0x6f, 0x55, 0x73, 0x65, 0x72, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x1d, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x17, 0x3a, 0x01,
	0x2a, 0x22, 0x12, 0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2f,
	0x75, 0x73, 0x65, 0x72, 0x73, 0x12, 0xf8, 0x01, 0x0a, 0x20, 0x52, 0x65, 0x63, 0x65, 0x69, 0x76,
	0x65, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x46, 0x72, 0x6f, 0x6d, 0x53, 0x65, 0x72,
	0x76, 0x65, 0x72, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x12, 0x50, 0x2e, 0x67, 0x69, 0x74,
	0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63,
	0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73,
	0x61, 0x67, 0x65, 0x73, 0x2e, 0x52, 0x65, 0x63, 0x65, 0x69, 0x76, 0x65, 0x4d, 0x65, 0x73, 0x73,
	0x61, 0x67, 0x65, 0x73, 0x46, 0x72, 0x6f, 0x6d, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x43, 0x68,
	0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x51, 0x2e, 0x67,
	0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74,
	0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x52, 0x65, 0x63, 0x65, 0x69, 0x76, 0x65, 0x4d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x46, 0x72, 0x6f, 0x6d, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72,
	0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22,
	0x2f, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x29, 0x12, 0x27, 0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x65, 0x73,
	0x73, 0x61, 0x67, 0x65, 0x73, 0x2f, 0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x73, 0x2f, 0x7b,
	0x73, 0x65, 0x72, 0x76, 0x65, 0x72, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x49, 0x64, 0x7d,
	0x12, 0xd5, 0x01, 0x0a, 0x17, 0x52, 0x65, 0x63, 0x65, 0x69, 0x76, 0x65, 0x4d, 0x65, 0x73, 0x73,
	0x61, 0x67, 0x65, 0x73, 0x46, 0x72, 0x6f, 0x6d, 0x55, 0x73, 0x65, 0x72, 0x12, 0x47, 0x2e, 0x67,
	0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74,
	0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x52, 0x65, 0x63, 0x65, 0x69, 0x76, 0x65, 0x4d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x46, 0x72, 0x6f, 0x6d, 0x55, 0x73, 0x65, 0x72, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x48, 0x2e, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63,
	0x6f, 0x6d, 0x2e, 0x6d, 0x73, 0x62, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x2e, 0x6d, 0x73, 0x62,
	0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x5f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e,
	0x52, 0x65, 0x63, 0x65, 0x69, 0x76, 0x65, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x46,
	0x72, 0x6f, 0x6d, 0x55, 0x73, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22,
	0x27, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x21, 0x12, 0x1f, 0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x65, 0x73,
	0x73, 0x61, 0x67, 0x65, 0x73, 0x2f, 0x75, 0x73, 0x65, 0x72, 0x73, 0x2f, 0x7b, 0x66, 0x72, 0x6f,
	0x6d, 0x55, 0x73, 0x65, 0x72, 0x49, 0x64, 0x7d, 0x42, 0xb8, 0x02, 0x92, 0x41, 0xa7, 0x02, 0x12,
	0xd5, 0x01, 0x0a, 0x1a, 0x4d, 0x53, 0x42, 0x69, 0x67, 0x74, 0x65, 0x63, 0x68, 0x20, 0x4d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x20, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x22, 0x58,
	0x0a, 0x14, 0x67, 0x52, 0x50, 0x43, 0x2d, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x20, 0x70,
	0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x12, 0x2e, 0x68, 0x74, 0x74, 0x70, 0x73, 0x3a, 0x2f, 0x2f,
	0x67, 0x69, 0x74, 0x68, 0x75, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x67, 0x72, 0x70, 0x63, 0x2d,
	0x65, 0x63, 0x6f, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x2f, 0x67, 0x72, 0x70, 0x63, 0x2d, 0x67,
	0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x1a, 0x10, 0x6e, 0x6f, 0x6e, 0x65, 0x40, 0x65, 0x78, 0x61,
	0x6d, 0x70, 0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d, 0x2a, 0x58, 0x0a, 0x14, 0x42, 0x53, 0x44, 0x20,
	0x33, 0x2d, 0x43, 0x6c, 0x61, 0x75, 0x73, 0x65, 0x20, 0x4c, 0x69, 0x63, 0x65, 0x6e, 0x73, 0x65,
	0x12, 0x40, 0x68, 0x74, 0x74, 0x70, 0x73, 0x3a, 0x2f, 0x2f, 0x67, 0x69, 0x74, 0x68, 0x75, 0x62,
	0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x67, 0x72, 0x70, 0x63, 0x2d, 0x65, 0x63, 0x6f, 0x73, 0x79, 0x73,
	0x74, 0x65, 0x6d, 0x2f, 0x67, 0x72, 0x70, 0x63, 0x2d, 0x67, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79,
	0x2f, 0x62, 0x6c, 0x6f, 0x62, 0x2f, 0x6d, 0x61, 0x69, 0x6e, 0x2f, 0x4c, 0x49, 0x43, 0x45, 0x4e,
	0x53, 0x45, 0x32, 0x03, 0x31, 0x2e, 0x30, 0x2a, 0x02, 0x01, 0x02, 0x72, 0x49, 0x0a, 0x17, 0x4d,
	0x6f, 0x72, 0x65, 0x20, 0x61, 0x62, 0x6f, 0x75, 0x74, 0x20, 0x67, 0x52, 0x50, 0x43, 0x2d, 0x47,
	0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x12, 0x2e, 0x68, 0x74, 0x74, 0x70, 0x73, 0x3a, 0x2f, 0x2f,
	0x67, 0x69, 0x74, 0x68, 0x75, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x67, 0x72, 0x70, 0x63, 0x2d,
	0x65, 0x63, 0x6f, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x2f, 0x67, 0x72, 0x70, 0x63, 0x2d, 0x67,
	0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x5a, 0x0b, 0x70, 0x6b, 0x67, 0x2f, 0x61, 0x70, 0x69, 0x3b,
	0x61, 0x70, 0x69, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var file_v1_service_proto_goTypes = []interface{}{
	(*CreateUserRequest)(nil),                        // 0: gitlab.com.msbigtech.msbigtech_messages.CreateUserRequest
	(*DeleteUserRequest)(nil),                        // 1: gitlab.com.msbigtech.msbigtech_messages.DeleteUserRequest
	(*CreateServerRequest)(nil),                      // 2: gitlab.com.msbigtech.msbigtech_messages.CreateServerRequest
	(*DeleteServerRequest)(nil),                      // 3: gitlab.com.msbigtech.msbigtech_messages.DeleteServerRequest
	(*CreateServerChannelRequest)(nil),               // 4: gitlab.com.msbigtech.msbigtech_messages.CreateServerChannelRequest
	(*DeleteServerChannelRequest)(nil),               // 5: gitlab.com.msbigtech.msbigtech_messages.DeleteServerChannelRequest
	(*InviteUserToServerRequest)(nil),                // 6: gitlab.com.msbigtech.msbigtech_messages.InviteUserToServerRequest
	(*SendMessageToServerChannelRequest)(nil),        // 7: gitlab.com.msbigtech.msbigtech_messages.SendMessageToServerChannelRequest
	(*SendMessageToUserRequest)(nil),                 // 8: gitlab.com.msbigtech.msbigtech_messages.SendMessageToUserRequest
	(*ReceiveMessagesFromServerChannelRequest)(nil),  // 9: gitlab.com.msbigtech.msbigtech_messages.ReceiveMessagesFromServerChannelRequest
	(*ReceiveMessagesFromUserRequest)(nil),           // 10: gitlab.com.msbigtech.msbigtech_messages.ReceiveMessagesFromUserRequest
	(*CreateUserResponse)(nil),                       // 11: gitlab.com.msbigtech.msbigtech_messages.CreateUserResponse
	(*DeleteUserResponse)(nil),                       // 12: gitlab.com.msbigtech.msbigtech_messages.DeleteUserResponse
	(*CreateServerResponse)(nil),                     // 13: gitlab.com.msbigtech.msbigtech_messages.CreateServerResponse
	(*DeleteServerResponse)(nil),                     // 14: gitlab.com.msbigtech.msbigtech_messages.DeleteServerResponse
	(*CreateServerChannelResponse)(nil),              // 15: gitlab.com.msbigtech.msbigtech_messages.CreateServerChannelResponse
	(*DeleteServerChannelResponse)(nil),              // 16: gitlab.com.msbigtech.msbigtech_messages.DeleteServerChannelResponse
	(*InviteUserToServerResponse)(nil),               // 17: gitlab.com.msbigtech.msbigtech_messages.InviteUserToServerResponse
	(*SendMessageToServerChannelResponse)(nil),       // 18: gitlab.com.msbigtech.msbigtech_messages.SendMessageToServerChannelResponse
	(*SendMessageToUserResponse)(nil),                // 19: gitlab.com.msbigtech.msbigtech_messages.SendMessageToUserResponse
	(*ReceiveMessagesFromServerChannelResponse)(nil), // 20: gitlab.com.msbigtech.msbigtech_messages.ReceiveMessagesFromServerChannelResponse
	(*ReceiveMessagesFromUserResponse)(nil),          // 21: gitlab.com.msbigtech.msbigtech_messages.ReceiveMessagesFromUserResponse
}
var file_v1_service_proto_depIdxs = []int32{
	0,  // 0: gitlab.com.msbigtech.msbigtech_messages.MessagesService.CreateUser:input_type -> gitlab.com.msbigtech.msbigtech_messages.CreateUserRequest
	1,  // 1: gitlab.com.msbigtech.msbigtech_messages.MessagesService.DeleteUser:input_type -> gitlab.com.msbigtech.msbigtech_messages.DeleteUserRequest
	2,  // 2: gitlab.com.msbigtech.msbigtech_messages.MessagesService.CreateServer:input_type -> gitlab.com.msbigtech.msbigtech_messages.CreateServerRequest
	3,  // 3: gitlab.com.msbigtech.msbigtech_messages.MessagesService.DeleteServer:input_type -> gitlab.com.msbigtech.msbigtech_messages.DeleteServerRequest
	4,  // 4: gitlab.com.msbigtech.msbigtech_messages.MessagesService.CreateServerChannel:input_type -> gitlab.com.msbigtech.msbigtech_messages.CreateServerChannelRequest
	5,  // 5: gitlab.com.msbigtech.msbigtech_messages.MessagesService.DeleteServerChannel:input_type -> gitlab.com.msbigtech.msbigtech_messages.DeleteServerChannelRequest
	6,  // 6: gitlab.com.msbigtech.msbigtech_messages.MessagesService.InviteUserToServer:input_type -> gitlab.com.msbigtech.msbigtech_messages.InviteUserToServerRequest
	7,  // 7: gitlab.com.msbigtech.msbigtech_messages.MessagesService.SendMessageToServerChannel:input_type -> gitlab.com.msbigtech.msbigtech_messages.SendMessageToServerChannelRequest
	8,  // 8: gitlab.com.msbigtech.msbigtech_messages.MessagesService.SendMessageToUser:input_type -> gitlab.com.msbigtech.msbigtech_messages.SendMessageToUserRequest
	9,  // 9: gitlab.com.msbigtech.msbigtech_messages.MessagesService.ReceiveMessagesFromServerChannel:input_type -> gitlab.com.msbigtech.msbigtech_messages.ReceiveMessagesFromServerChannelRequest
	10, // 10: gitlab.com.msbigtech.msbigtech_messages.MessagesService.ReceiveMessagesFromUser:input_type -> gitlab.com.msbigtech.msbigtech_messages.ReceiveMessagesFromUserRequest
	11, // 11: gitlab.com.msbigtech.msbigtech_messages.MessagesService.CreateUser:output_type -> gitlab.com.msbigtech.msbigtech_messages.CreateUserResponse
	12, // 12: gitlab.com.msbigtech.msbigtech_messages.MessagesService.DeleteUser:output_type -> gitlab.com.msbigtech.msbigtech_messages.DeleteUserResponse
	13, // 13: gitlab.com.msbigtech.msbigtech_messages.MessagesService.CreateServer:output_type -> gitlab.com.msbigtech.msbigtech_messages.CreateServerResponse
	14, // 14: gitlab.com.msbigtech.msbigtech_messages.MessagesService.DeleteServer:output_type -> gitlab.com.msbigtech.msbigtech_messages.DeleteServerResponse
	15, // 15: gitlab.com.msbigtech.msbigtech_messages.MessagesService.CreateServerChannel:output_type -> gitlab.com.msbigtech.msbigtech_messages.CreateServerChannelResponse
	16, // 16: gitlab.com.msbigtech.msbigtech_messages.MessagesService.DeleteServerChannel:output_type -> gitlab.com.msbigtech.msbigtech_messages.DeleteServerChannelResponse
	17, // 17: gitlab.com.msbigtech.msbigtech_messages.MessagesService.InviteUserToServer:output_type -> gitlab.com.msbigtech.msbigtech_messages.InviteUserToServerResponse
	18, // 18: gitlab.com.msbigtech.msbigtech_messages.MessagesService.SendMessageToServerChannel:output_type -> gitlab.com.msbigtech.msbigtech_messages.SendMessageToServerChannelResponse
	19, // 19: gitlab.com.msbigtech.msbigtech_messages.MessagesService.SendMessageToUser:output_type -> gitlab.com.msbigtech.msbigtech_messages.SendMessageToUserResponse
	20, // 20: gitlab.com.msbigtech.msbigtech_messages.MessagesService.ReceiveMessagesFromServerChannel:output_type -> gitlab.com.msbigtech.msbigtech_messages.ReceiveMessagesFromServerChannelResponse
	21, // 21: gitlab.com.msbigtech.msbigtech_messages.MessagesService.ReceiveMessagesFromUser:output_type -> gitlab.com.msbigtech.msbigtech_messages.ReceiveMessagesFromUserResponse
	11, // [11:22] is the sub-list for method output_type
	0,  // [0:11] is the sub-list for method input_type
	0,  // [0:0] is the sub-list for extension type_name
	0,  // [0:0] is the sub-list for extension extendee
	0,  // [0:0] is the sub-list for field type_name
}

func init() { file_v1_service_proto_init() }
func file_v1_service_proto_init() {
	if File_v1_service_proto != nil {
		return
	}
	file_v1_messages_proto_init()
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_v1_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   0,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_v1_service_proto_goTypes,
		DependencyIndexes: file_v1_service_proto_depIdxs,
	}.Build()
	File_v1_service_proto = out.File
	file_v1_service_proto_rawDesc = nil
	file_v1_service_proto_goTypes = nil
	file_v1_service_proto_depIdxs = nil
}
