package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/swaggo/files"
	_ "github.com/swaggo/gin-swagger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"net/http"
	"os"
)

// @title           MSBigtech Messages API
// @version         1.0

// @host      localhost:8080
// @BasePath /hc
// @schemes http https

// @tag.name healthchecks
// @tag.description Healthchecks API

// @securitydefinitions.oauth2.implicit MSBigtechAuth
// @authorizationUrl https://msbigtech.local/oauth/authorize

// @securitydefinitions.apikey api_key
// @in header
// @name api_key

// LivenessProbeHandler godoc
// @Summary      liveness probe
// @Tags         healthchecks
// @Accept       json
// @Produce      json
// @Success      200  {string}  string    "ok"
// @Router       /live [get]
func LivenessProbeHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"response": "ok",
	})
}

// ReadinessProbeHandler godoc
// @Summary      readiness probe
// @Tags         healthchecks
// @Accept       json
// @Produce      json
// @Success      200  {string}  string    "ok"
// @Router       /ready [get]
func ReadinessProbeHandler(c *gin.Context) {
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=prefer TimeZone=Europe/Moscow",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_NAME"),
		os.Getenv("DB_PORT"),
	)
	_, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}

	c.JSON(http.StatusOK, gin.H{
		"response": "ok",
	})
}
