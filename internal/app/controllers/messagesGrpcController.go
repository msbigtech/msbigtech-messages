package controllers

import (
	"buf.build/gen/go/bufbuild/protovalidate/protocolbuffers/go/buf/validate"
	"context"
	"errors"
	"fmt"
	"github.com/bufbuild/protovalidate-go"
	"github.com/mehdihadeli/go-mediatr"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	apiv1 "msbigtech/api/gen/v1"
	"msbigtech/internal/app/usecases/cmds"
	"msbigtech/internal/app/usecases/queries"
	"sync"
)

type MessagesGrpcController struct {
	apiv1.UnimplementedMessagesServiceServer

	mx        sync.RWMutex
	validator *protovalidate.Validator
}

func NewMessagesGrpcController() (*MessagesGrpcController, error) {
	srv := &MessagesGrpcController{}

	validator, err := protovalidate.New(
		protovalidate.WithDisableLazy(true),
		protovalidate.WithMessages(
			// Добавляем сюда все запросы наши
			&apiv1.CreateUserRequest{},
			&apiv1.DeleteUserRequest{},
			&apiv1.CreateServerRequest{},
			&apiv1.DeleteServerRequest{},
			&apiv1.CreateServerChannelRequest{},
			&apiv1.DeleteServerChannelRequest{},
			&apiv1.InviteUserToServerRequest{},
			&apiv1.SendMessageToServerChannelRequest{},
			&apiv1.SendMessageToUserRequest{},
			&apiv1.ReceiveMessagesFromServerChannelRequest{},
			&apiv1.ReceiveMessagesFromUserRequest{},
		),
	)

	if err != nil {
		return nil, fmt.Errorf("failed to initialize validator: %w", err)
	}

	srv.validator = validator

	return srv, nil
}

func (c *MessagesGrpcController) CreateUser(
	ctx context.Context,
	req *apiv1.CreateUserRequest,
) (*apiv1.CreateUserResponse, error) {
	if err := c.validator.Validate(req); err != nil {
		return nil, rpcValidationError(err)
	}

	command := &cmds.CreateUserCommand{
		CurrentUserId: req.GetCurrUserId(),
		Id:            req.GetId(),
		Email:         req.GetEmail(),
		Nickname:      req.GetNickname(),
	}

	_, err := mediatr.Send[*cmds.CreateUserCommand, *cmds.CqrsGenericCommandResponse](ctx, command)
	if err != nil {
		return nil, err
	}

	return &apiv1.CreateUserResponse{}, nil
}

func (c *MessagesGrpcController) DeleteUser(
	ctx context.Context,
	req *apiv1.DeleteUserRequest,
) (*apiv1.DeleteUserResponse, error) {
	if err := c.validator.Validate(req); err != nil {
		return nil, rpcValidationError(err)
	}

	command := &cmds.DeleteUserCommand{
		CurrentUserId: req.GetCurrUserId(),
		Id:            req.GetId(),
	}

	_, err := mediatr.Send[*cmds.DeleteUserCommand, *cmds.CqrsGenericCommandResponse](ctx, command)
	if err != nil {
		return nil, err
	}

	return &apiv1.DeleteUserResponse{}, nil
}

func (c *MessagesGrpcController) CreateServer(
	ctx context.Context,
	req *apiv1.CreateServerRequest,
) (*apiv1.CreateServerResponse, error) {
	if err := c.validator.Validate(req); err != nil {
		return nil, rpcValidationError(err)
	}

	command := &cmds.CreateServerCommand{
		CurrentUserId: req.GetCurrUserId(),
		Id:            req.GetId(),
		Name:          req.GetName(),
	}

	_, err := mediatr.Send[*cmds.CreateServerCommand, *cmds.CqrsGenericCommandResponse](ctx, command)
	if err != nil {
		return nil, err
	}

	return &apiv1.CreateServerResponse{}, nil
}

func (c *MessagesGrpcController) DeleteServer(
	ctx context.Context,
	req *apiv1.DeleteServerRequest,
) (*apiv1.DeleteServerResponse, error) {
	if err := c.validator.Validate(req); err != nil {
		return nil, rpcValidationError(err)
	}

	command := &cmds.DeleteServerCommand{
		CurrentUserId: req.GetCurrUserId(),
		Id:            req.GetId(),
	}

	_, err := mediatr.Send[*cmds.DeleteServerCommand, *cmds.CqrsGenericCommandResponse](ctx, command)
	if err != nil {
		return nil, err
	}

	return &apiv1.DeleteServerResponse{}, nil
}

func (c *MessagesGrpcController) CreateServerChannel(
	ctx context.Context,
	req *apiv1.CreateServerChannelRequest,
) (*apiv1.CreateServerChannelResponse, error) {
	if err := c.validator.Validate(req); err != nil {
		return nil, rpcValidationError(err)
	}

	command := &cmds.CreateServerChannelCommand{
		CurrentUserId: req.GetCurrUserId(),
		Id:            req.GetId(),
		Name:          req.GetName(),
	}

	_, err := mediatr.Send[*cmds.CreateServerChannelCommand, *cmds.CqrsGenericCommandResponse](ctx, command)
	if err != nil {
		return nil, err
	}

	return &apiv1.CreateServerChannelResponse{}, nil
}

func (c *MessagesGrpcController) DeleteServerChannel(
	ctx context.Context,
	req *apiv1.DeleteServerChannelRequest,
) (*apiv1.DeleteServerChannelResponse, error) {
	if err := c.validator.Validate(req); err != nil {
		return nil, rpcValidationError(err)
	}

	command := &cmds.DeleteServerChannelCommand{
		CurrentUserId: req.GetCurrUserId(),
		Id:            req.GetId(),
	}

	_, err := mediatr.Send[*cmds.DeleteServerChannelCommand, *cmds.CqrsGenericCommandResponse](ctx, command)
	if err != nil {
		return nil, err
	}

	return &apiv1.DeleteServerChannelResponse{}, nil
}

func (c *MessagesGrpcController) InviteUserToServer(
	ctx context.Context,
	req *apiv1.InviteUserToServerRequest,
) (*apiv1.InviteUserToServerResponse, error) {
	if err := c.validator.Validate(req); err != nil {
		return nil, rpcValidationError(err)
	}

	command := &cmds.InviteUserToServerCommand{
		CurrentUserId:   req.GetCurrUserId(),
		ToId:            req.GetToUserId(),
		ServerChannelId: req.GetServerChannelId(),
	}

	_, err := mediatr.Send[*cmds.InviteUserToServerCommand, *cmds.CqrsGenericCommandResponse](ctx, command)
	if err != nil {
		return nil, err
	}

	return &apiv1.InviteUserToServerResponse{}, nil
}

func (c *MessagesGrpcController) SendMessageToServerChannel(
	ctx context.Context,
	req *apiv1.SendMessageToServerChannelRequest,
) (*apiv1.SendMessageToServerChannelResponse, error) {
	if err := c.validator.Validate(req); err != nil {
		return nil, rpcValidationError(err)
	}

	command := &cmds.SendMessageToServerChannelCommand{
		CurrentUserId:   req.GetCurrUserId(),
		ServerChannelId: req.GetServerChannelId(),
		Text:            req.GetMessage(),
	}

	_, err := mediatr.Send[*cmds.SendMessageToServerChannelCommand, *cmds.CqrsGenericCommandResponse](ctx, command)

	if err != nil {
		return nil, err
	}

	return &apiv1.SendMessageToServerChannelResponse{}, nil
}

func (c *MessagesGrpcController) SendMessageToUser(
	ctx context.Context,
	req *apiv1.SendMessageToUserRequest,
) (*apiv1.SendMessageToUserResponse, error) {
	if err := c.validator.Validate(req); err != nil {
		return nil, rpcValidationError(err)
	}

	command := &cmds.SendMessageToUserCommand{
		CurrentUserId: req.GetCurrUserId(),
		ToId:          req.GetToUserId(),
		Text:          req.GetMessage(),
	}

	_, err := mediatr.Send[*cmds.SendMessageToUserCommand, *cmds.CqrsGenericCommandResponse](ctx, command)

	if err != nil {
		return nil, err
	}

	return &apiv1.SendMessageToUserResponse{}, nil
}

func (c *MessagesGrpcController) ReceiveMessagesFromServerChannel(
	ctx context.Context,
	req *apiv1.ReceiveMessagesFromServerChannelRequest,
) (*apiv1.ReceiveMessagesFromServerChannelResponse, error) {
	if err := c.validator.Validate(req); err != nil {
		return nil, rpcValidationError(err)
	}

	query := &queries.ReceiveMessagesFromServerChannelQuery{
		CurrentUserId:   req.GetCurrUserId(),
		ServerChannelId: req.GetServerChannelId(),
	}

	result, err := mediatr.Send[
		*queries.ReceiveMessagesFromServerChannelQuery,
		*queries.ReceiveMessagesFromServerChannelQueryResponse,
	](ctx, query)

	if err != nil {
		return nil, err
	}

	messages := make([]*apiv1.ReceiveMessagesFromServerChannelResponse_ServerChannelMessage, 0, len(result.Items))

	for _, item := range result.Items {
		messages = append(messages, &apiv1.ReceiveMessagesFromServerChannelResponse_ServerChannelMessage{
			Id:   item.ToId,
			Text: item.Text,
		})
	}

	return &apiv1.ReceiveMessagesFromServerChannelResponse{
		Messages: messages,
	}, nil
}

func (c *MessagesGrpcController) ReceiveMessagesFromUser(
	ctx context.Context,
	req *apiv1.ReceiveMessagesFromUserRequest,
) (*apiv1.ReceiveMessagesFromUserResponse, error) {
	if err := c.validator.Validate(req); err != nil {
		return nil, rpcValidationError(err)
	}

	query := &queries.ReceiveMessagesFromUserQuery{
		CurrentUserId: req.GetCurrUserId(),
		FromUserId:    req.GetFromUserId(),
	}

	result, err := mediatr.Send[
		*queries.ReceiveMessagesFromUserQuery,
		*queries.ReceiveMessagesFromUserQueryResponse,
	](ctx, query)

	if err != nil {
		return nil, err
	}

	messages := make([]*apiv1.ReceiveMessagesFromUserResponse_UserMessage, 0, len(result.Items))

	for _, item := range result.Items {
		messages = append(messages, &apiv1.ReceiveMessagesFromUserResponse_UserMessage{
			Id:   item.ToId,
			Text: item.Text,
		})
	}

	return &apiv1.ReceiveMessagesFromUserResponse{
		Messages: messages,
	}, nil
}

// validation

func protovalidateViolationsToGoogleViolations(
	vs []*validate.Violation,
) []*errdetails.BadRequest_FieldViolation {
	res := make([]*errdetails.BadRequest_FieldViolation, len(vs))
	for i, v := range vs {
		res[i] = &errdetails.BadRequest_FieldViolation{
			Field:       v.FieldPath,
			Description: v.Message,
		}
	}
	return res
}

func convertProtovalidateValidationErrorToErrdetailsBadRequest(
	valErr *protovalidate.ValidationError,
) *errdetails.BadRequest {
	return &errdetails.BadRequest{
		FieldViolations: protovalidateViolationsToGoogleViolations(valErr.Violations),
	}
}

func rpcValidationError(err error) error {
	if err == nil {
		return nil
	}

	var valErr *protovalidate.ValidationError
	if ok := errors.As(err, &valErr); ok {
		st, err := status.New(codes.InvalidArgument, codes.InvalidArgument.String()).
			WithDetails(convertProtovalidateValidationErrorToErrdetailsBadRequest(valErr))
		if err == nil {
			return st.Err()
		}
	}

	return status.Error(codes.Internal, err.Error())
}
