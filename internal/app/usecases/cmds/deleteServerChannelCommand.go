package cmds

type DeleteServerChannelCommand struct {
	CurrentUserId uint64
	Id            uint64
}
