package cmds

type SendMessageToServerChannelCommand struct {
	CurrentUserId   uint64
	ServerChannelId uint64
	Text            string
}
