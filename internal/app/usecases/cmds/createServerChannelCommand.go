package cmds

type CreateServerChannelCommand struct {
	CurrentUserId uint64
	Id            uint64
	Name          string
}
