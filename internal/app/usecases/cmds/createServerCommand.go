package cmds

type CreateServerCommand struct {
	CurrentUserId uint64
	Id            uint64
	Name          string
}
