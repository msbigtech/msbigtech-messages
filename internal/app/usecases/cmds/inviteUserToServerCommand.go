package cmds

type InviteUserToServerCommand struct {
	CurrentUserId   uint64
	ToId            uint64
	ServerChannelId uint64
}
