package cmds

type SendMessageToUserCommand struct {
	CurrentUserId uint64
	ToId          uint64
	Text          string
}
