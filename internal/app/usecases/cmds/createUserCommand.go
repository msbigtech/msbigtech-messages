package cmds

type CreateUserCommand struct {
	CurrentUserId uint64
	Id            uint64
	Email         string
	Nickname      string
}
