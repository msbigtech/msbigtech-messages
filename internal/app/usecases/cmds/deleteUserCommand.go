package cmds

type DeleteUserCommand struct {
	CurrentUserId uint64
	Id            uint64
}
