package cmds

type DeleteServerCommand struct {
	CurrentUserId uint64
	Id            uint64
}
