package queries

import (
	"msbigtech/internal/app/entities"
)

type ReceiveMessagesFromUserQuery struct {
	CurrentUserId uint64
	FromUserId    uint64
}

type ReceiveMessagesFromUserQueryResponse struct {
	Items []entities.MessageModel
}
