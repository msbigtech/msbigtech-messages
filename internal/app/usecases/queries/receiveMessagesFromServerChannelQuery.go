package queries

import (
	"msbigtech/internal/app/entities"
)

type ReceiveMessagesFromServerChannelQuery struct {
	CurrentUserId   uint64
	ServerChannelId uint64
}

type ReceiveMessagesFromServerChannelQueryResponse struct {
	Items []entities.MessageModel
}
