package cqrshandlers

import (
	"context"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/usecases/cmds"
)

type CreateServerChannelCommandHandler struct {
	serverChannelsRepository interfaces.ServerChannelsRepositoryInterface
}

func NewCreateServerChannelCommandHandler(
	serverChannelsRepository interfaces.ServerChannelsRepositoryInterface,
) *CreateServerChannelCommandHandler {
	return &CreateServerChannelCommandHandler{
		serverChannelsRepository: serverChannelsRepository,
	}
}

func (h *CreateServerChannelCommandHandler) Handle(
	ctx context.Context,
	command *cmds.CreateServerChannelCommand,
) (*cmds.CqrsGenericCommandResponse, error) {
	_, err := h.serverChannelsRepository.Create(
		command.Id,
		command.Name,
		command.CurrentUserId)

	if err != nil {
		return nil, err
	}

	return &cmds.CqrsGenericCommandResponse{}, nil
}
