package cqrshandlers

import (
	"context"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/usecases/cmds"
)

type CreateUserCommandHandler struct {
	usersRepository interfaces.UsersRepositoryInterface
}

func NewCreateUserCommandHandler(
	usersRepository interfaces.UsersRepositoryInterface,
) *CreateUserCommandHandler {
	return &CreateUserCommandHandler{usersRepository: usersRepository}
}

func (h *CreateUserCommandHandler) Handle(
	ctx context.Context,
	command *cmds.CreateUserCommand,
) (*cmds.CqrsGenericCommandResponse, error) {
	_, err := h.usersRepository.Create(
		command.Id,
		command.Nickname,
		command.Email,
	)

	if err != nil {
		return nil, err
	}

	return &cmds.CqrsGenericCommandResponse{}, nil
}
