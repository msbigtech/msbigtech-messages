package cqrshandlers

import (
	"context"
	"msbigtech/internal/app/entities"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/usecases/cmds"
)

type SendMessageToUserCommandHandler struct {
	messagesRepository interfaces.MessagesRepositoryInterface
}

func NewSendMessageToUserCommandHandler(
	messagesRepository interfaces.MessagesRepositoryInterface,
) *SendMessageToUserCommandHandler {
	return &SendMessageToUserCommandHandler{
		messagesRepository: messagesRepository,
	}
}

func (h *SendMessageToUserCommandHandler) Handle(
	ctx context.Context,
	command *cmds.SendMessageToUserCommand,
) (*cmds.CqrsGenericCommandResponse, error) {
	_, err := h.messagesRepository.Create(
		0,
		command.ToId,
		command.CurrentUserId,
		entities.UserToUser,
		command.Text)

	if err != nil {
		return nil, err
	}

	return &cmds.CqrsGenericCommandResponse{}, nil
}
