package cqrshandlers

import (
	"context"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/usecases/cmds"
)

type DeleteServerCommandHandler struct {
	serversRepository interfaces.ServersRepositoryInterface
}

func NewDeleteServerCommandHandler(
	serversRepository interfaces.ServersRepositoryInterface,
) *DeleteServerCommandHandler {
	return &DeleteServerCommandHandler{serversRepository: serversRepository}
}

func (h *DeleteServerCommandHandler) Handle(
	ctx context.Context,
	command *cmds.DeleteServerCommand,
) (*cmds.CqrsGenericCommandResponse, error) {
	err := h.serversRepository.Delete(command.Id)

	if err != nil {
		return nil, err
	}

	return &cmds.CqrsGenericCommandResponse{}, nil
}
