package cqrshandlers

import (
	"context"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/usecases/cmds"
)

type DeleteServerChannelCommandHandler struct {
	serverChannelsRepository interfaces.ServerChannelsRepositoryInterface
}

func NewDeleteServerChannelCommandHandler(
	serverChannelsRepository interfaces.ServerChannelsRepositoryInterface,
) *DeleteServerChannelCommandHandler {
	return &DeleteServerChannelCommandHandler{serverChannelsRepository: serverChannelsRepository}
}

func (h *DeleteServerChannelCommandHandler) Handle(
	ctx context.Context,
	command *cmds.DeleteServerChannelCommand,
) (*cmds.CqrsGenericCommandResponse, error) {
	err := h.serverChannelsRepository.Delete(command.Id)

	if err != nil {
		return nil, err
	}

	return &cmds.CqrsGenericCommandResponse{}, nil
}
