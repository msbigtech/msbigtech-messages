package cqrshandlers

import (
	"context"
	"msbigtech/internal/app/entities"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/usecases/cmds"
)

type SendMessageToServerChannelCommandHandler struct {
	messagesRepository interfaces.MessagesRepositoryInterface
}

func NewSendMessageToServerChannelCommandHandler(
	messagesRepository interfaces.MessagesRepositoryInterface,
) *SendMessageToServerChannelCommandHandler {
	return &SendMessageToServerChannelCommandHandler{
		messagesRepository: messagesRepository,
	}
}

func (h *SendMessageToServerChannelCommandHandler) Handle(
	ctx context.Context,
	command *cmds.SendMessageToServerChannelCommand,
) (*cmds.CqrsGenericCommandResponse, error) {
	_, err := h.messagesRepository.Create(
		0,
		command.ServerChannelId,
		command.CurrentUserId,
		entities.UserToChannel,
		command.Text)

	if err != nil {
		return nil, err
	}

	return &cmds.CqrsGenericCommandResponse{}, nil
}
