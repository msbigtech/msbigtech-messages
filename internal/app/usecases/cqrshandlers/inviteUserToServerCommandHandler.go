package cqrshandlers

import (
	"context"
	"msbigtech/internal/app/entities"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/usecases/cmds"
)

type InviteUserToServerCommandHandler struct {
	messagesRepository interfaces.MessagesRepositoryInterface
}

func NewInviteUserToServerCommandHandler(
	messagesRepository interfaces.MessagesRepositoryInterface,
) *InviteUserToServerCommandHandler {
	return &InviteUserToServerCommandHandler{
		messagesRepository: messagesRepository,
	}
}

func (h *InviteUserToServerCommandHandler) Handle(
	ctx context.Context,
	command *cmds.InviteUserToServerCommand,
) (*cmds.CqrsGenericCommandResponse, error) {
	_, err := h.messagesRepository.Create(
		0,
		command.ServerChannelId,
		command.CurrentUserId,
		entities.UserInvite,
		"")

	if err != nil {
		return nil, err
	}

	return &cmds.CqrsGenericCommandResponse{}, nil
}
