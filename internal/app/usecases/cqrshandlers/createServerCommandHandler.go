package cqrshandlers

import (
	"context"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/usecases/cmds"
)

type CreateServerCommandHandler struct {
	serversRepository interfaces.ServersRepositoryInterface
}

func NewCreateServerCommandHandler(
	serversRepository interfaces.ServersRepositoryInterface,
) *CreateServerCommandHandler {
	return &CreateServerCommandHandler{serversRepository: serversRepository}
}

func (h *CreateServerCommandHandler) Handle(
	ctx context.Context,
	command *cmds.CreateServerCommand,
) (*cmds.CqrsGenericCommandResponse, error) {
	_, err := h.serversRepository.Create(
		command.Id,
		command.Name,
		command.CurrentUserId)

	if err != nil {
		return nil, err
	}

	return &cmds.CqrsGenericCommandResponse{}, nil
}
