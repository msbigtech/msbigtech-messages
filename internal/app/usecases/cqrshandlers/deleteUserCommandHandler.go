package cqrshandlers

import (
	"context"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/usecases/cmds"
)

type DeleteUserCommandHandler struct {
	usersRepository interfaces.UsersRepositoryInterface
}

func NewDeleteUserCommandHandler(
	usersRepository interfaces.UsersRepositoryInterface,
) *DeleteUserCommandHandler {
	return &DeleteUserCommandHandler{usersRepository: usersRepository}
}

func (h *DeleteUserCommandHandler) Handle(
	ctx context.Context,
	command *cmds.DeleteUserCommand,
) (*cmds.CqrsGenericCommandResponse, error) {
	err := h.usersRepository.Delete(command.Id)

	if err != nil {
		return nil, err
	}

	return &cmds.CqrsGenericCommandResponse{}, nil
}
