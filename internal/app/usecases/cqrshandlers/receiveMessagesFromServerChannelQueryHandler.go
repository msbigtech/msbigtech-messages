package cqrshandlers

import (
	"context"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/usecases/queries"
)

type ReceiveMessagesFromServerChannelQueryHandler struct {
	messagesRepository interfaces.MessagesRepositoryInterface
}

func NewReceiveMessagesFromServerChannelQueryHandler(
	messagesRepository interfaces.MessagesRepositoryInterface,
) *ReceiveMessagesFromServerChannelQueryHandler {
	return &ReceiveMessagesFromServerChannelQueryHandler{messagesRepository: messagesRepository}
}

func (h *ReceiveMessagesFromServerChannelQueryHandler) Handle(
	ctx context.Context,
	query *queries.ReceiveMessagesFromServerChannelQuery,
) (*queries.ReceiveMessagesFromServerChannelQueryResponse, error) {
	messages, err := h.messagesRepository.GetByChannelId(query.ServerChannelId)

	if err != nil {
		return nil, err
	}

	return &queries.ReceiveMessagesFromServerChannelQueryResponse{
		Items: *messages,
	}, nil
}
