package cqrshandlers

import (
	"context"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/usecases/queries"
)

type ReceiveMessagesFromUserQueryHandler struct {
	messagesRepository interfaces.MessagesRepositoryInterface
}

func NewReceiveMessagesFromUserQueryHandler(
	messagesRepository interfaces.MessagesRepositoryInterface,
) *ReceiveMessagesFromUserQueryHandler {
	return &ReceiveMessagesFromUserQueryHandler{messagesRepository: messagesRepository}
}

func (h *ReceiveMessagesFromUserQueryHandler) Handle(
	ctx context.Context,
	query *queries.ReceiveMessagesFromUserQuery,
) (*queries.ReceiveMessagesFromUserQueryResponse, error) {
	messages, err := h.messagesRepository.GetByUser(query.FromUserId, query.CurrentUserId)

	if err != nil {
		return nil, err
	}

	return &queries.ReceiveMessagesFromUserQueryResponse{
		Items: *messages,
	}, nil
}
