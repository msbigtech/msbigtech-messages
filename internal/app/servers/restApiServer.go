package servers

import (
	"github.com/gin-gonic/gin"
	rkboot "github.com/rookie-ninja/rk-boot/v2"
	rkgin "github.com/rookie-ninja/rk-gin/v2/boot"
	"msbigtech/internal/app/controllers"
)

func NewRestApiServer() *rkboot.Boot {
	// Create a new boot instance.
	boot := rkboot.NewBoot()

	// Register handler
	entry := rkgin.GetGinEntry("restapi")

	entry.Router.Use(gin.Logger())

	entry.Router.GET("/hc/live", controllers.LivenessProbeHandler)
	entry.Router.GET("/hc/ready", controllers.ReadinessProbeHandler)

	return boot
}
