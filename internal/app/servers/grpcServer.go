package servers

import (
	"github.com/mehdihadeli/go-mediatr"
	rkentry "github.com/rookie-ninja/rk-entry/v2/entry"
	rkgrpc "github.com/rookie-ninja/rk-grpc/v2/boot"
	"google.golang.org/grpc"
	"log"
	api "msbigtech/api/gen/v1"
	"msbigtech/internal/app/controllers"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/usecases/cmds"
	"msbigtech/internal/app/usecases/cqrshandlers"
	"msbigtech/internal/app/usecases/queries"
	"os"
)

func registerCqrsRequestHandlers(
	serverChannelsRepository interfaces.ServerChannelsRepositoryInterface,
	serversRepository interfaces.ServersRepositoryInterface,
	usersRepository interfaces.UsersRepositoryInterface,
	messagesRepository interfaces.MessagesRepositoryInterface,
) {
	mediatr.RegisterRequestHandler[
		*cmds.CreateServerChannelCommand,
		*cmds.CqrsGenericCommandResponse,
	](cqrshandlers.NewCreateServerChannelCommandHandler(serverChannelsRepository))

	mediatr.RegisterRequestHandler[
		*cmds.CreateServerCommand,
		*cmds.CqrsGenericCommandResponse,
	](cqrshandlers.NewCreateServerCommandHandler(serversRepository))

	mediatr.RegisterRequestHandler[
		*cmds.CreateUserCommand,
		*cmds.CqrsGenericCommandResponse,
	](cqrshandlers.NewCreateUserCommandHandler(usersRepository))

	mediatr.RegisterRequestHandler[
		*cmds.DeleteServerChannelCommand,
		*cmds.CqrsGenericCommandResponse,
	](cqrshandlers.NewDeleteServerChannelCommandHandler(serverChannelsRepository))

	mediatr.RegisterRequestHandler[
		*cmds.DeleteServerCommand,
		*cmds.CqrsGenericCommandResponse,
	](cqrshandlers.NewDeleteServerCommandHandler(serversRepository))

	mediatr.RegisterRequestHandler[
		*cmds.DeleteUserCommand,
		*cmds.CqrsGenericCommandResponse,
	](cqrshandlers.NewDeleteUserCommandHandler(usersRepository))

	mediatr.RegisterRequestHandler[
		*cmds.InviteUserToServerCommand,
		*cmds.CqrsGenericCommandResponse,
	](cqrshandlers.NewInviteUserToServerCommandHandler(messagesRepository))

	mediatr.RegisterRequestHandler[
		*cmds.SendMessageToServerChannelCommand,
		*cmds.CqrsGenericCommandResponse,
	](cqrshandlers.NewSendMessageToServerChannelCommandHandler(messagesRepository))

	mediatr.RegisterRequestHandler[
		*cmds.SendMessageToUserCommand,
		*cmds.CqrsGenericCommandResponse,
	](cqrshandlers.NewSendMessageToUserCommandHandler(messagesRepository))

	mediatr.RegisterRequestHandler[
		*queries.ReceiveMessagesFromUserQuery,
		*queries.ReceiveMessagesFromUserQueryResponse,
	](cqrshandlers.NewReceiveMessagesFromUserQueryHandler(messagesRepository))

	mediatr.RegisterRequestHandler[
		*queries.ReceiveMessagesFromServerChannelQuery,
		*queries.ReceiveMessagesFromServerChannelQueryResponse,
	](cqrshandlers.NewReceiveMessagesFromServerChannelQueryHandler(messagesRepository))
}

func NewGrpcServer(
	controller *controllers.MessagesGrpcController,
	serverChannelsRepository interfaces.ServerChannelsRepositoryInterface,
	serversRepository interfaces.ServersRepositoryInterface,
	usersRepository interfaces.UsersRepositoryInterface,
	messagesRepository interfaces.MessagesRepositoryInterface,
) *rkgrpc.GrpcEntry {
	bootgrpc, err := os.ReadFile("boot-grpc.yaml")
	if err != nil {
		log.Fatal(err)
	}

	rkentry.BootstrapBuiltInEntryFromYAML(bootgrpc)
	rkentry.BootstrapPluginEntryFromYAML(bootgrpc)

	// Bootstrap grpc entry from boot config
	res := rkgrpc.RegisterGrpcEntryYAML(bootgrpc)

	// Register CQRS handlers
	registerCqrsRequestHandlers(
		serverChannelsRepository,
		serversRepository,
		usersRepository,
		messagesRepository,
	)

	// Get GrpcEntry
	grpcEntry := res["messages"].(*rkgrpc.GrpcEntry)

	// Register gRPC server
	grpcEntry.AddRegFuncGrpc(func(server *grpc.Server) {
		api.RegisterMessagesServiceServer(server, controller)
	})

	// Register grpc-gateway func
	grpcEntry.AddRegFuncGw(api.RegisterMessagesServiceHandlerFromEndpoint)

	return grpcEntry
}
