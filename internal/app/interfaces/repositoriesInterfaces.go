package interfaces

import (
	"msbigtech/internal/app/entities"
)

type UsersRepositoryInterface interface {
	Create(externalId uint64, name string, email string) (*entities.UserModel, error)
	GetById(id uint64) (*entities.UserModel, error)
	GetByExternalId(externalId uint64) (*entities.UserModel, error)
	Delete(id uint64) error
}

type ServersRepositoryInterface interface {
	Create(externalId uint64, name string, ownerId uint64) (*entities.ServerModel, error)
	GetById(id uint64) (*entities.ServerModel, error)
	GetByExternalId(externalId uint64) (*entities.ServerModel, error)
	Delete(id uint64) error
}

type ServerChannelsRepositoryInterface interface {
	Create(externalId uint64, name string, ownerId uint64) (*entities.ServerChannelModel, error)
	GetById(id uint64) (*entities.ServerChannelModel, error)
	GetByExternalId(externalId uint64) (*entities.ServerChannelModel, error)
	Delete(id uint64) error
}

type MessagesRepositoryInterface interface {
	Create(id uint64, toId uint64, ownerId uint64, messageType entities.MessageModelType, text string) (*entities.MessageModel, error)
	GetById(id uint64) (*entities.MessageModel, error)
	GetByExternalId(externalId uint64) (*entities.MessageModel, error)
	GetByUser(userId uint64, ownerId uint64) (*[]entities.MessageModel, error)
	GetByChannelId(channelId uint64) (*[]entities.MessageModel, error)
	Delete(id uint64) error
}
