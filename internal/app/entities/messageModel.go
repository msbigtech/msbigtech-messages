package entities

const (
	UserToUser    MessageModelType = 1
	UserToChannel MessageModelType = 2
	UserInvite    MessageModelType = 3
)

type MessageModelType int

type MessageModel struct {
	OwnerId uint64
	ToId    uint64
	Text    string
	Type    MessageModelType
}
