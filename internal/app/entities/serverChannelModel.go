package entities

type ServerChannelModel struct {
	Id         uint64
	ExternalId uint64
	Name       string
	OwnerId    uint64
}
