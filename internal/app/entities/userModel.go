package entities

type UserModel struct {
	Id         uint64
	ExternalId uint64
	Name       string
	Email      string
}
