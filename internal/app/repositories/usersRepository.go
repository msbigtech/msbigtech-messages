package repositories

import (
	"msbigtech/internal/app/entities"
	"msbigtech/internal/app/interfaces"
)

func NewUsersRepository() interfaces.UsersRepositoryInterface {
	return &userRepository{}
}

type userRepository struct {
}

func (c *userRepository) Create(externalId uint64, name string, email string) (*entities.UserModel, error) {
	return &entities.UserModel{}, nil
}

func (c *userRepository) GetById(id uint64) (*entities.UserModel, error) {
	return &entities.UserModel{}, nil
}

func (c *userRepository) GetByExternalId(externalId uint64) (*entities.UserModel, error) {
	return &entities.UserModel{}, nil
}

func (c *userRepository) Delete(id uint64) error {
	return nil
}
