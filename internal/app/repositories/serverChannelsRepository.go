package repositories

import (
	"msbigtech/internal/app/entities"
	"msbigtech/internal/app/interfaces"
)

func NewServerChannelsRepository() interfaces.ServerChannelsRepositoryInterface {
	return &serverChannelsRepository{}
}

type serverChannelsRepository struct {
}

func (c *serverChannelsRepository) Create(externalId uint64, name string, ownerId uint64) (*entities.ServerChannelModel, error) {
	return &entities.ServerChannelModel{}, nil
}

func (c *serverChannelsRepository) GetById(id uint64) (*entities.ServerChannelModel, error) {
	return &entities.ServerChannelModel{}, nil
}

func (c *serverChannelsRepository) GetByExternalId(externalId uint64) (*entities.ServerChannelModel, error) {
	return &entities.ServerChannelModel{}, nil
}

func (c *serverChannelsRepository) Delete(id uint64) error {
	return nil
}
