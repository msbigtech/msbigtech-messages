package repositories

import (
	"msbigtech/internal/app/entities"
	"msbigtech/internal/app/interfaces"
)

func NewMessagesRepository() interfaces.MessagesRepositoryInterface {
	return &messagesRepository{}
}

type messagesRepository struct {
}

func (r *messagesRepository) Create(id uint64, toId uint64, ownerId uint64, messageType entities.MessageModelType, text string) (*entities.MessageModel, error) {
	return nil, nil
}

func (r *messagesRepository) GetById(id uint64) (*entities.MessageModel, error) {
	return nil, nil
}

func (r *messagesRepository) GetByExternalId(externalId uint64) (*entities.MessageModel, error) {
	return nil, nil
}

func (r *messagesRepository) Delete(id uint64) error {
	return nil
}

func (r *messagesRepository) GetByUser(userId uint64, ownerId uint64) (*[]entities.MessageModel, error) {
	return nil, nil
}

func (r *messagesRepository) GetByChannelId(channelId uint64) (*[]entities.MessageModel, error) {
	return nil, nil
}
