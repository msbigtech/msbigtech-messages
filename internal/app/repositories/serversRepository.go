package repositories

import (
	"msbigtech/internal/app/entities"
	"msbigtech/internal/app/interfaces"
)

func NewServersRepository() interfaces.ServersRepositoryInterface {
	return &serverRepository{}
}

type serverRepository struct {
}

func (c *serverRepository) Create(externalId uint64, name string, ownerId uint64) (*entities.ServerModel, error) {
	return &entities.ServerModel{}, nil
}

func (c *serverRepository) GetById(id uint64) (*entities.ServerModel, error) {
	return &entities.ServerModel{}, nil
}

func (c *serverRepository) GetByExternalId(externalId uint64) (*entities.ServerModel, error) {
	return &entities.ServerModel{}, nil
}

func (c *serverRepository) Delete(id uint64) error {
	return nil
}
