package repositories

import "go.uber.org/fx"

var FxRepositoriesModule = fx.Options(
	fx.Provide(NewUsersRepository),
	fx.Provide(NewServersRepository),
	fx.Provide(NewServerChannelsRepository),
	fx.Provide(NewMessagesRepository),
)
