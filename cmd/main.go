package main

import (
	"context"
	"github.com/joho/godotenv"
	rkboot "github.com/rookie-ninja/rk-boot/v2"
	rkentry "github.com/rookie-ninja/rk-entry/v2/entry"
	rkgrpc "github.com/rookie-ninja/rk-grpc/v2/boot"
	"go.uber.org/fx"
	"msbigtech/internal/app/controllers"
	"msbigtech/internal/app/interfaces"
	"msbigtech/internal/app/repositories"
	"msbigtech/internal/app/servers"
)

func GinServer(
	lc fx.Lifecycle,
	controller *controllers.MessagesGrpcController,
	serverChannelsRepository interfaces.ServerChannelsRepositoryInterface,
	serversRepository interfaces.ServersRepositoryInterface,
	usersRepository interfaces.UsersRepositoryInterface,
	messagesRepository interfaces.MessagesRepositoryInterface,
) (*rkboot.Boot, *rkgrpc.GrpcEntry) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	restApiServer := servers.NewRestApiServer()
	grpcServer := servers.NewGrpcServer(
		controller,
		serverChannelsRepository,
		serversRepository,
		usersRepository,
		messagesRepository,
	)

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			// Bootstrap servers
			go restApiServer.Bootstrap(ctx)
			go grpcServer.Bootstrap(ctx)

			// Wait for shutdown signal
			go rkentry.GlobalAppCtx.WaitForShutdownSig()

			return nil
		},
		OnStop: func(ctx context.Context) error {
			restApiServer.Shutdown(ctx)
			grpcServer.Interrupt(ctx)

			return nil
		},
	})

	return restApiServer, grpcServer
}

func main() {
	// TODO: вынести загрузку конфигов в отдельный класс
	godotenv.Load(".env")

	app := fx.New(
		repositories.FxRepositoriesModule,
		fx.Provide(controllers.NewMessagesGrpcController),
		fx.Provide(GinServer),
		fx.Invoke(func(*rkboot.Boot, *rkgrpc.GrpcEntry) {}),
	)

	app.Run()
}
